﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tzVideoteka.Models;
using PagedList.Mvc;
using PagedList;

namespace tzVideoteka.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Список фильмов.";

            if (DataInstance.Instanse.GetFilms().Count == 0)
                LoadObj();

            return View();
        }

        public ActionResult GetFilms(int? page, string SortOrder, string Filter)
        {
            int pageSize = 2;
            int pageNumber = (page ?? 1);

            List<FilmModel> films = DataInstance.Instanse.GetFilms().OrderBy(p => p.Name).ToList();

            ViewBag.NameOrder = ((SortOrder == "Name desc" || String.IsNullOrEmpty(SortOrder)) ? "Name" : "Name desc");
            ViewBag.DateOrder = ((SortOrder == "Date desc" || String.IsNullOrEmpty(SortOrder)) ? "Date" : "Date desc");
            ViewBag.AuthorOrder = ((SortOrder == "Author desc" || String.IsNullOrEmpty(SortOrder)) ? "Author" : "Author desc");
            ViewBag.Filter = Filter;

            switch (SortOrder)
            {
                
                case "Name desc":
                    {
                        films = films.OrderByDescending(p => p.Name).ToList();
                        break;
                    }
                case "Author desc":
                    {
                        films = films.OrderByDescending(p => p.UserAutor).ToList();
                        break;
                    }
                case "Author":
                    {
                        films = films.OrderBy(p => p.UserAutor).ToList();
                        break;
                    }
                case "Date":
                    {
                        films = films.OrderBy(p => p.ReleaseDate).ToList();
                        break;
                    }
                case "Date desc":
                    {
                        films = films.OrderByDescending(p => p.ReleaseDate).ToList();
                        break;
                    }                    

            }
            
            if (!String.IsNullOrEmpty(Filter))
                films = DataInstance.Instanse.GetFilms().Where(p => p.Name.Contains(Filter)).ToList();

            return PartialView("GetFilms", films.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult GetFilms(string filter, int? page)
        {
            int pageSize = 2;
            int pageNumber = (page ?? 1);

            ViewBag.Filter = filter;
            List<FilmModel> films = DataInstance.Instanse.GetFilms().Where(p => p.Name.Contains(filter)).ToList();

            return PartialView("GetFilms", films.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int ID)
        {
            var item = DataInstance.Instanse.GetFilmByID(ID);

            return View(item);
        }

        public ActionResult Create()
        {
            if (UsersContext.GetUserName() == "")
                return RedirectToAction("Login", "Account");            

            return View();
        }

        [HttpPost]
        public ActionResult Create(FilmModel model)
        {
            if (UsersContext.GetUserName() == "")
            {
                ModelState.AddModelError("", "Пожалуйста, авторизуйтесь!");
                return View(model);
            }

            if (!ModelState.IsValid)
                return View(model);

            int ID = DataInstance.Instanse.Create(model, ModelState);

            if (ID != 0)
                return Redirect("/Home/Details?ID="+ ID);

            return View(model);
        }

        public ActionResult Edit(int ID)
        {
            if (UsersContext.GetUserName() == "")
                return RedirectToAction("Login", "Account");

            FilmModel item = DataInstance.Instanse.GetFilmByID(ID);

            if (item == null)
            {
                ModelState.AddModelError("", "Выбранная запись не найдена.");
                return View(new FilmModel() { ID = 0 });
            }

            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(FilmModel model, int ID)
        {
            if (UsersContext.GetUserName() == "")
            {
                ModelState.AddModelError("", "Пожалуйста, авторизуйтесь!");
                return View(model);
            }

            if (!ModelState.IsValid)
                return View(model);

            FilmModel item = DataInstance.Instanse.GetFilmByID(ID);            

            if (item == null)
            {
                ModelState.AddModelError("", "Выбранная запись не найдена.");
                return View(new FilmModel() { ID = 0 });
            }

            if (UsersContext.GetUserName() != item.UserAutor)
            {
                ModelState.AddModelError("", "запись может редактировать только ее автор!");
                return View(model);
            }

            if (!DataInstance.Instanse.Edit(model, ModelState))
                return View(model);

            return Redirect("/Home/Details?ID=" + ID);
        }

        void LoadObj()        
        {
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм1", Descr = "нерально крутой экшен", DirectedBy = "Квентин Тарантино", ReleaseDate = DateTime.Now.AddYears(-1), UserAutor = "User1" }, null);
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм2", Descr = "нерально крутой экшен", DirectedBy = "Квентин Тарантино1", ReleaseDate = DateTime.Now.AddYears(-2), UserAutor = "User2" }, null);
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм3", Descr = "нерально крутой экшен", DirectedBy = "Квентин Тарантино2", ReleaseDate = DateTime.Now.AddYears(-3), UserAutor = "User3" }, null);
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм4", Descr = "нерально крутой экшен", DirectedBy = "Михалков Н.С.", ReleaseDate = DateTime.Now.AddYears(-5), UserAutor = "User2" }, null);
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм5", Descr = "нерально крутой экшен", DirectedBy = "Говорухин С.С.", ReleaseDate = DateTime.Now.AddYears(-1), UserAutor = "User1" }, null);
            DataInstance.Instanse.Create(new FilmModel() { Name = "Самый лучший фильм6", Descr = "нерально крутой экшен", DirectedBy = "Говорухин С.С.", ReleaseDate = DateTime.Now.AddYears(-1), UserAutor = "User1" }, null);
        }
       
    }
}
