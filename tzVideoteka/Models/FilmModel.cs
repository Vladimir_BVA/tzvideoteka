﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace tzVideoteka.Models
{
    public class FilmModel
    {
        public int ID { get; set; }
        [Display(Name = "Автор")]
        public string UserAutor { get; set; }
        [Display(Name = "Режисер")]
        [Required(ErrorMessage = "Введите значение.")]
        public string DirectedBy { get; set; }
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Введите значение.")]
        public string Name { get; set; }
        [Display(Name = "Примечание")]
        [MaxLength(250, ErrorMessage = "Максимальное значение 250 символов.")]
        public string Descr { get; set; }
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата выпуска")]
        [Required(ErrorMessage = "Выберите значение.")]
        public DateTime ReleaseDate { get; set; }
    }
}