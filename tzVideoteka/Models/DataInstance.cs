﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tzVideoteka.Models
{
    public class DataInstance
    {
        List<FilmModel> listModel = new List<FilmModel>();

        public static DataInstance Instanse = new DataInstance();

        public List<FilmModel> GetFilms()
        {
            return this.listModel.OrderBy(p => p.Name).ToList();
        }

        public FilmModel GetFilmByID(int ID)
        {
            return this.listModel.Find(p => p.ID == ID);
        }        

        public int Create(FilmModel model, System.Web.Mvc.ModelStateDictionary modelState)
        {
            if (!CheckModel(model))
            {        
                modelState.AddModelError("", "Данный фильм уже был добавлен ранее.");
                return 0;
            }

            if(listModel.Count > 0)
                model.ID = listModel.Max(p => p.ID)+1;
            else
                model.ID = 1;
            
            if(string.IsNullOrEmpty(model.UserAutor))
                model.UserAutor = UsersContext.GetUserName();

            listModel.Add(model);

            return model.ID;
        }

        public bool Edit(FilmModel newModel, System.Web.Mvc.ModelStateDictionary modelState)
        {
            FilmModel lastModel = this.GetFilmByID(newModel.ID);

            if (lastModel == null)
            {
                modelState.AddModelError("", "Запись отсутствует.");
                return false;
            }            

            lock (listModel)
            {
                listModel.RemoveAt(listModel.IndexOf(lastModel));
                newModel.UserAutor = UsersContext.GetUserName();

                listModel.Add(newModel);
            }

            return true;
        }

        bool CheckModel(FilmModel model)
        {
            FilmModel check = listModel.Find(p => p.Name == model.Name && p.ReleaseDate == model.ReleaseDate);

            if (check != null)
                return false;

            return true;
        }       
    }
}